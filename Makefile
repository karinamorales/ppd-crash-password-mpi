all: compile
	
compile:
	gcc -o 4 4.thread.c -lpthread -lm
	mpicc -o 6 6.MPI.c
	gcc -o ascii tabela_ascii.c

clean:
	rm ex?
	rm -rf *.out

